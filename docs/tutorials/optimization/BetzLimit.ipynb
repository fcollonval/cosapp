{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](../images/cosapp.svg)\n",
    "\n",
    "**CoSApp** tutorials on optimization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimizing an Actuator Disk Model to Find Betz Limit for Wind Turbines\n",
    "\n",
    "**Preliminary note:**\n",
    "\n",
    "This case is taken from [OpenMDAO](https://openmdao.org/newdocs/versions/latest/examples/betz_limit.html). OpenMDAO is an open-source computing platform for system analysis and multidisciplinary optimization developed by the NASA. Its philosophy shares some of the goals of CoSApp. Thus, this example is also an opportunity to compare both libraries.\n",
    "\n",
    "OpenMDAO is licensed under [Apache License](https://github.com/OpenMDAO/OpenMDAO/blob/master/LICENSE.txt).\n",
    "\n",
    "## Case description\n",
    "\n",
    "The Betz limit is the theoretical maximum amount of kinetic energy that a wind turbine can extract from the flow. This limit was derived analytically by Albert Betz in 1919, but it can also be found numerically using an optimizer and a simple actuator disk model for a wind turbine.\n",
    "\n",
    "## Creating the elements"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "\n",
    "class ActuatorDisc(System):\n",
    "    \"\"\"Simple wind turbine model based on actuator disc theory\n",
    "    \"\"\"\n",
    "    def setup(self):\n",
    "        # Inputs\n",
    "        self.add_inward('a', 0.5, desc=\"Induced Velocity Factor\")\n",
    "        self.add_inward('area', 10.0, unit=\"m**2\", desc=\"Rotor disc area\")\n",
    "        self.add_inward('rho', 1.225, unit=\"kg/m**3\", desc=\"air density\")\n",
    "        self.add_inward('Vu', 10.0, unit=\"m/s\",\n",
    "            desc=\"Freestream air velocity, upstream of rotor\")\n",
    "        # Outputs\n",
    "        self.add_outward('Vr', 0.0, unit=\"m/s\",\n",
    "            desc=\"Air velocity at rotor exit plane\")\n",
    "        self.add_outward('Vd', 0.0, unit=\"m/s\",\n",
    "            desc=\"Slipstream air velocity, downstream of rotor\")\n",
    "        self.add_outward('Ct', 0.0, desc=\"Thrust Coefficient\")\n",
    "        self.add_outward('thrust', 0.0, unit=\"N\",\n",
    "            desc=\"Thrust produced by the rotor\")\n",
    "        self.add_outward('Cp', 0.0, desc=\"Power Coefficient\")\n",
    "        self.add_outward('power', 0.0, unit=\"W\",\n",
    "            desc=\"Power produced by the rotor\")\n",
    "\n",
    "    def compute(self):\n",
    "        \"\"\"Considering the entire rotor as a single disc that extracts\n",
    "        velocity uniformly from the incoming flow and converts it to power.\n",
    "        \"\"\"\n",
    "        a = self.a\n",
    "        Vu = self.Vu\n",
    "\n",
    "        qA = 0.5 * self.rho * self.area * Vu**2\n",
    "\n",
    "        self.Vd = Vd = Vu * (1 - 2 * a)\n",
    "        self.Vr = 0.5 * (Vu + Vd)\n",
    "\n",
    "        self.Ct = Ct = 4 * a * (1 - a)\n",
    "        self.thrust = Ct * qA\n",
    "\n",
    "        self.Cp = Cp = Ct * (1 - a)\n",
    "        self.power = Cp * qA * Vu\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting the system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build model\n",
    "disc = ActuatorDisc('disc')\n",
    "\n",
    "disc.a = 1.0\n",
    "disc.area = 0.1\n",
    "disc.rho = 1.225\n",
    "disc.Vu = 10.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solving the optimization problem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import Optimizer\n",
    "\n",
    "# Setup optimization problem\n",
    "optim = disc.add_driver(Optimizer('optim', method='SLSQP'))\n",
    "\n",
    "min_area = 1\n",
    "max_area = 10\n",
    "\n",
    "optim.add_unknown('a', lower_bound=0, upper_bound=1)\n",
    "optim.add_unknown('area', lower_bound=min_area, upper_bound=max_area)\n",
    "optim.set_maximum('power')\n",
    "\n",
    "# Initialize unknowns\n",
    "disc.a = 0.1\n",
    "disc.area = 1.0\n",
    "\n",
    "disc.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Validation \n",
    "\n",
    "Comparison of numerical solution *vs.* exact optimal values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rel_error(actual, expected):\n",
    "    return abs(actual / expected - 1)\n",
    "\n",
    "optimum = {\n",
    "    'a': 1 / 3,\n",
    "    'Cp': 16 / 27,\n",
    "    'area': 10,\n",
    "}\n",
    "\n",
    "solution = dict(disc.inwards.items())\n",
    "solution.update(disc.outwards.items())\n",
    "\n",
    "for varname, exact in optimum.items():\n",
    "    actual = disc[varname]\n",
    "    print(f\"disc.{varname} = {actual}\\t(error = {rel_error(actual, exact)})\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import itertools, copy\n",
    "\n",
    "axes = {\n",
    "    'a': np.linspace(0, 1, 21),\n",
    "    'area': np.linspace(min_area, max_area, 19),\n",
    "}\n",
    "x = axes['a']\n",
    "y = axes['area']\n",
    "z = []\n",
    "\n",
    "# Create new system to avoid side effects\n",
    "s = ActuatorDisc('s')\n",
    "for varname, value in disc.inwards.items():\n",
    "    s[varname] = copy.copy(value)\n",
    "\n",
    "for s.a, s.area in itertools.product(x, y):\n",
    "    s.run_once()\n",
    "    z.append(s.power)\n",
    "\n",
    "z = np.reshape(z, (x.size, -1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot results\n",
    "import plotly.graph_objects as go\n",
    "\n",
    "template = '<br>'.join([\n",
    "    'a: %{x}',\n",
    "    'area: %{y}',\n",
    "    'power: %{z:.1f}',\n",
    "])\n",
    "\n",
    "traces = [\n",
    "    go.Surface(\n",
    "        x = axes['a'],\n",
    "        y = axes['area'],\n",
    "        z = z.T,\n",
    "        name = 'power',\n",
    "        hovertemplate = template + '<extra></extra>',\n",
    "        colorscale = 'balance',\n",
    "    ),\n",
    "    go.Scatter3d(\n",
    "        x = [solution['a']],\n",
    "        y = [solution['area']],\n",
    "        z = [solution['power']],\n",
    "        name = 'solution',\n",
    "        marker=dict(size=6, opacity=1, color='red'),\n",
    "        hovertemplate = template,\n",
    "    ),\n",
    "]\n",
    "\n",
    "fig = go.Figure(data=traces)\n",
    "\n",
    "fig.update_layout(\n",
    "    scene = {\n",
    "        \"xaxis\": {\"title\": \"a\"},\n",
    "        \"yaxis\": {\"title\": \"area\"},\n",
    "        \"zaxis\": {\"title\": \"power\"},\n",
    "    },\n",
    "    height = 600,\n",
    ")\n",
    "\n",
    "fig.show()"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
